type threshold is nat

type max_proposal is nat

type max_message_size is nat

type state_hash is bytes

type addr_set is set (address)

type message_store is map (bytes, addr_set)

type proposal_counters is map (address, nat)

type storage is
  record [
    state_hash : state_hash;
    threshold : threshold;
    max_proposal : max_proposal;
    max_message_size : max_message_size;
    authorized_addresses : addr_set;
    message_store : message_store;
    proposal_counters : proposal_counters
  ]

type message is bytes -> list (operation)

type send_pt is message

type withdraw_pt is message

type default_pt is unit

type return is list (operation) * storage

type parameter is
    Send of send_pt
  | Withdraw of withdraw_pt
  | Default of default_pt

function send (const param : send_pt; const s : storage)
  : return is
block {
  if not Set.mem (Tezos.sender, s.authorized_addresses)
  then failwith ("Unauthorized address")
  else skip;
  var message : message := param;
  const packed_msg : bytes = Bytes.pack (message);
  if Bytes.length (packed_msg) > s.max_message_size
  then failwith ("Message size exceed maximum limit")
  else skip;
  var new_store : addr_set := set [];
  case map_get (packed_msg, s.message_store) of [
    Some (voters) ->
      block {
        if Set.mem (Tezos.sender, voters)
        then skip
        else
          s.proposal_counters [Tezos.sender] :=
            get_force (Tezos.sender, s.proposal_counters)
            + 1n;
        new_store := Set.add (Tezos.sender, voters)
      }
  | None ->
      block {
        s.proposal_counters [sender] :=
          get_force (Tezos.sender, s.proposal_counters) + 1n;
        new_store := set [Tezos.sender]
      }
  ];
  var sender_proposal_counter : nat
  := get_force (Tezos.sender, s.proposal_counters);
  if sender_proposal_counter > s.max_proposal
  then failwith ("Maximum number of proposal reached")
  else skip;
  var ret_ops : list (operation) := nil;
  if Set.cardinal (new_store) >= s.threshold
  then {
    remove packed_msg from map s.message_store;
    ret_ops := message (s.state_hash);
    s.state_hash :=
      Crypto.sha256
        (Bytes.concat (s.state_hash, packed_msg));
    for addr -> ctr in map s.proposal_counters
    block {
      if Set.mem (addr, new_store)
      then s.proposal_counters [addr] := abs (ctr - 1n)
      else skip
    }
  }
  else s.message_store [packed_msg] := new_store
} with (ret_ops, s)

function withdraw
  (const param : withdraw_pt;
   const s : storage) : return is
block {
  var message : message := param;
  const packed_msg : bytes = Bytes.pack (message);
  case s.message_store [packed_msg] of [
    Some (voters) ->
      block {
        const new_set : addr_set
        = Set.remove (Tezos.sender, voters);
        if Set.cardinal (voters) =/= Set.cardinal (new_set)
        then
          s.proposal_counters [Tezos.sender] :=
            abs
              (get_force (Tezos.sender, s.proposal_counters)
               - 1n)
        else skip;
        if Set.cardinal (new_set) = 0n
        then remove packed_msg from map s.message_store
        else s.message_store [packed_msg] := new_set
      }
  | None -> skip
  ]
} with ((nil : list (operation)), s)

function default (const p : default_pt; const s : storage)
  : return is ((nil : list (operation)), s)

function main (const param : parameter; const s : storage)
  : return is
  case param of [
    Send (p) -> send (p, s)
  | Withdraw (p) -> withdraw (p, s)
  | Default (p) -> default (p, s)
  ]
