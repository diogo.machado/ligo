# TODO: remove this as submodules aren't used anymore.
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  LIGO_REGISTRY_IMAGE_BASE_NAME: "${CI_PROJECT_PATH}/${CI_PROJECT_NAME}"
  WEBIDE_IMAGE_NAME: "registry.gitlab.com/${CI_PROJECT_PATH}/ligo_webide"

stages:
  - build
  - push
  - ide-deploy
  - versioning

.docker-image:
  stage: push
  image: docker:19.03.5
  services:
    - docker:19.03.5-dind

version_scheduled_job:
  stage: versioning
  script:
    - scripts/versioning.sh
  only:
    - schedules

.nix:
  stage: build
  tags:
    - nix
  before_script:
    - find "$CI_PROJECT_DIR" -path "$CI_PROJECT_DIR/.git" -prune -o "(" -type d -a -not -perm -u=w ")" -exec chmod --verbose u+w {} ";"
    - nix-env -f channel:nixos-unstable -iA gnutar gitMinimal
    - export COMMIT_DATE="$(git show --no-patch --format=%ci)"

# The binary produced is useless by itself
binary:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
  script:
    - nix-build nix -A ligo-bin

doc:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
  script:
    - nix-build nix -A ligo-doc
    - cp -Lr --no-preserve=mode,ownership,timestamps result/share/doc .
  artifacts:
    paths:
      - doc

test:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
  script:
    - nix-build nix -A ligo-coverage
    - cat result/share/coverage-all
    - cp -Lr --no-preserve=mode,ownership,timestamps result/share/coverage .
  artifacts:
    paths:
      - coverage

xrefcheck:
  extends: .nix
  only:
    - merge_requests
  script:
    # Should be replaced with
    # nix run github:serokell/xrefcheck
    # Once flakes roll out to stable
    # - nix run -f https://github.com/serokell/xrefcheck/archive/v0.1.1.2.tar.gz -c 'xrefcheck local-only'
    - curl -L https://github.com/serokell/xrefcheck/releases/download/v0.1.1/release.tar.gz | tar -zxf - xrefcheck/bin/xrefcheck
    - xrefcheck/bin/xrefcheck

# Strange race conditions, disable for now
.webide-e2e:
  extends: .nix
  only:
    # Disabled for now unless the branch name contains webide, because a test in this job fails randomly
    - /.*webide.*/
    #- merge_requests
    #- dev
    #- /^.*-run-dev$/
  script:
    - nix-build nix -A ligo-editor.e2e

docker:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
  script:
    - nix-build nix -A ligo-docker
    - cp -L result ligo.tar.gz
  artifacts:
    paths:
      - ligo.tar.gz

docker-push:
  extends: .docker-image
  dependencies:
    - docker
  needs:
    - docker
  rules:
    # Only deploy docker when from the dev branch AND on the canonical ligolang/ligo repository
    - if: '$CI_COMMIT_REF_NAME =~ /^(dev|.*-run-dev)$/ && $CI_PROJECT_PATH == "ligolang/ligo"'
      when: always
  script:
    - echo ${LIGO_REGISTRY_PASSWORD} | docker login -u ${LIGO_REGISTRY_USER} --password-stdin
    - docker load -i=./ligo.tar.gz
    - export LIGO_REGISTRY_FULL_NAME=${LIGO_REGISTRY_IMAGE_BUILD:-ligolang/ligo}:$(if test "$CI_COMMIT_REF_NAME" = "dev"; then echo next; else echo next-attempt; fi)
    - docker tag ligo "${LIGO_REGISTRY_FULL_NAME}"
    - docker push "${LIGO_REGISTRY_FULL_NAME}"

webide-docker:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
  script:
    - nix-build nix -A ligo-editor-docker
    - cp -L result webide.tar.gz
  artifacts:
    paths:
      - webide.tar.gz

webide-push:
  extends: .docker-image
  dependencies:
    - webide-docker
  needs:
    - webide-docker
  rules:
    # Only deploy docker when from the dev branch AND on the canonical ligolang/ligo repository
    - if: '$CI_COMMIT_REF_NAME =~ /^(dev|.*-run-dev)$/ && $CI_PROJECT_PATH == "ligolang/ligo"'
      when: always
  script:
    - echo "${CI_BUILD_TOKEN}" | docker login -u gitlab-ci-token --password-stdin registry.gitlab.com
    - docker load -i=./webide.tar.gz
    - docker tag ligo-editor "${WEBIDE_IMAGE_NAME}:${CI_COMMIT_SHORT_SHA}"
    - docker push "${WEBIDE_IMAGE_NAME}:${CI_COMMIT_SHORT_SHA}"

deploy-handoff:
  # Handoff deployment duties to private repo
  stage: ide-deploy
  variables:
    IDE_DOCKER_IMAGE: "registry.gitlab.com/${CI_PROJECT_PATH}/ligo_webide"
    LIGO_COMMIT_REF_NAME: "${CI_COMMIT_SHORT_SHA}"
  trigger: ligolang/ligo-webide-deploy
  rules:
    - if: '$CI_COMMIT_REF_NAME == "dev"'
      when: always

static-binary:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
  script:
    - nix-build nix -A ligo-static
    # Check that the binary is truly static and has 0 dependencies
    - test $(nix-store -q --references ./result | wc -l) -eq 0
    - cp -L result/bin/ligo ligo
    - chmod +rwx ligo
  artifacts:
    paths:
      - ligo

.website:
  extends: .nix
  script:
    - nix-build nix -A ligo-website
    - cp -Lr --no-preserve=mode,ownership,timestamps result/ public
  artifacts:
    paths:
      - public

pages:
  extends: .website
  rules:
    - if: '$CI_COMMIT_REF_NAME == "dev" && $CI_PROJECT_PATH == "ligolang/ligo"'
      when: always

pages-attempt:
  extends: .website
  only:
    - merge_requests
    - /^.*-run-dev$/
